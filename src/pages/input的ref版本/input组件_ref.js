import React, { Component } from 'react'
import Filed from './index_ref';
export default class App extends Component {
  userName = React.createRef();
  passWord = React.createRef();
  render() {
    return (
      <div>
        <div>登录</div>
        <Filed type="text" label="用户名" ref={this.userName}></Filed>
        <Filed type="password" label="密码" ref={this.passWord}></Filed>
        <button onClick={()=>{
          console.log(this.userName.current.state.values)
        }}>登录</button>
        <button onClick={()=>{
          this.userName.current.clearn()
          this.passWord.current.clearn()
        }}>清空</button>
      </div>
    )
  }
}
