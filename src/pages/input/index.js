import React, { Component } from 'react'

export default class Filed extends Component {
    getValue=(e)=>{
        this.props.valueData(e.target.value);
    }
    render() {
        return (
            <div>
                <span>{this.props.label}</span>
                <input type={this.props.type} onInput={this.getValue} value={this.props.value} />
            </div>
        )
    }
}
