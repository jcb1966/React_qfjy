import React, { Component } from 'react'
import axios from 'axios';
import FilemItem from './FilemItem';
import FileDetail from './FileDetail';
import './index.css';


// 状态提升 （中间人）模式 解决兄弟组件通讯
export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
            detail:'',
        }
        axios.get(`/test.json`).then((res) => {
            console.log(res.data.data.films);
            this.setState({
                filmList: res.data.data.films
            })
        })
    }
    getInfo = (e)=>{
        console.log(e);
        this.setState({
            detail:e
        })
    }
    render() {
        return (
            <div className='content_wrap'>
                <div className='left_wrap'>
                    {
                        this.state.filmList.map(item => {
                            return <FilemItem {...item} key={item.filmId} selectItem={this.getInfo} ></FilemItem>
                        })
                    }
                </div>
                <div className='right_wrap'>
                    <FileDetail detail={this.state.detail}></FileDetail>
                </div>
            </div>
        )
    }
}
