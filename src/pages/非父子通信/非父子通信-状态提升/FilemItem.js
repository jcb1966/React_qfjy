import React, { Component } from 'react'

export default class FilemItem extends Component {
  handleItem  = ()=>{
    this.props.selectItem(this.props.synopsis)
  }
  render() {
      
    return (
      <div className='movie_wrap' onClick={this.handleItem}>
          <h2>{this.props.name}</h2>
          <img src={this.props.poster} alt="this.props.name" />
      </div>
    )
  }
}
